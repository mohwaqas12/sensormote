/*
 * Utils_Test.c
 *
 *  Created on: May 1, 2019
 *      Author: waqas
 */

#include "unity.h"
#include "unity_fixture.h"

#include "utils.h"

TEST_GROUP(Utils);

#define VAR1_VALUE "45"
#define VAR1_DEF "200"
#define VAR1_NAME "VAR1"


TEST_SETUP(Utils)
{
}

TEST_TEAR_DOWN(Utils)
{
}

TEST(Utils, Test_ReadEnv)
{
	//Set Some ENVs and try  reading

	TEST_ASSERT_EQUAL_STRING( NULL , ReadEnv(NULL,NULL));
	//Try reading var default value
	TEST_ASSERT_EQUAL_STRING( VAR1_DEF , ReadEnv(VAR1_NAME,VAR1_DEF));
	//Set value and read again
	setenv(VAR1_NAME, VAR1_VALUE,0);
	TEST_ASSERT_EQUAL_STRING( VAR1_VALUE , ReadEnv(VAR1_NAME,VAR1_DEF));

}

