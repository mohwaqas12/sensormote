/*
 * transport_Test.c
 *
 *  Created on: May 2, 2019
 *      Author: waqas
 */

#include "unity.h"
#include "unity_fixture.h"

#include "transport.h"

TEST_GROUP(Transport);


TEST_SETUP(Transport)
{
}

TEST_TEAR_DOWN(Transport)
{
}

TEST(Transport, Test_TransportInit)
{
	//Allocate transport and test if its not null
	Transport_t *transport = TransportInit();
	TEST_ASSERT_NOT_NULL(transport);

	TransportDeinit(&transport);
	TEST_ASSERT_NULL(transport);
}

TEST(Transport, Test_TransportDeInit)
{
	Transport_t *transport = NULL;
	//Call with a NULL pointer
	TransportDeinit(NULL);
	TransportDeinit(&transport);
	TEST_ASSERT_NULL(transport);
}

TEST(Transport, Test_TransportOpen)
{
	Transport_t *transport =  TransportInit();
	char host[] = "localhost";
	int port = 12345;

	//Test parameters with NULLs
	TEST_ASSERT_EQUAL(-1, TransportOpen(NULL,NULL,0));
	TEST_ASSERT_EQUAL(-1, TransportOpen(NULL,NULL,port));
	TEST_ASSERT_EQUAL(-1, TransportOpen(transport,NULL,port));

	//Pass right values , still should fail to connect
	TEST_ASSERT_NOT_EQUAL(0, TransportOpen(transport,host,port));

	//Deallocate
	TransportDeinit(&transport);
	TEST_ASSERT_NULL(transport);
}

TEST(Transport, Test_TransportSend)
{
	//TODO: Later

}

TEST(Transport, Test_TransportClose)
{
	Transport_t *transport =  TransportInit();
	TEST_ASSERT_NOT_NULL(transport);
	TEST_ASSERT_NOT_EQUAL(0, TransportClose(NULL));

	TEST_ASSERT_NOT_EQUAL(-1, TransportClose(transport));

	//Deallocate
	TransportDeinit(&transport);
	TEST_ASSERT_NULL(transport);
}

