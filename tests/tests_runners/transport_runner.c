/*
 * transport_runner.c
 *
 *  Created on: May 2, 2019
 *      Author: waqas
 */

#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP_RUNNER(Transport)
{
  RUN_TEST_CASE(Transport, Test_TransportInit);
  RUN_TEST_CASE(Transport, Test_TransportDeInit);
  RUN_TEST_CASE(Transport, Test_TransportOpen);
  RUN_TEST_CASE(Transport, Test_TransportSend);
  RUN_TEST_CASE(Transport, Test_TransportClose);

}

