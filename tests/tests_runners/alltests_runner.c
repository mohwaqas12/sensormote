/*
 * alltests.c
 *
 *  Created on: May 1, 2019
 *      Author: waqas
 */

#include "unity_fixture.h"

static void RunAllTests(void)
{
  RUN_TEST_GROUP(Utils);
  RUN_TEST_GROUP(Transport);
}

int main(int argc, const char * argv[])
{
  return UnityMain(argc, argv, RunAllTests);

}
