BUILD_DIR=build
TARGET = $(BUILD_DIR)/sensormote

INCLUDES =  -Isrc/include/ -Isrc/

ifeq ($(RPI),1)
CFLAGS = -g -Wall -DRPI
else
CFLAGS = -g -Wall
endif 

SRC = src/utils.c \
      src/sensormote.c \
	  src/logging.c \
	  src/tempsensor.c \
	  src/transport.c

all: $(TARGET)

run: $(TARGET)
	$(TARGET)
	
$(TARGET): $(SRC)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(TARGET) $(SRC)

install : $(TARGET)
	install -m 755 $(TARGET) /usr/local/bin/
	install -d /usr/local/lib/systemd/system
	echo "Installing service"
	install -m 644 src/scripts/sensormote.service /usr/local/lib/systemd/system/
	systemctl daemon-reload
	echo "Service enabled"
	systemctl enable sensormote.service
	echo "Service started"
	systemctl start sensormote


clean:
	rm -rf $(TARGET)