#Sensormote
This contains sensormote source written in C
Sensormote reads the temperature and send it over a TCP socket
All output goes to syslog 
By default all data is sent to localhost on port 5005 

Sensormote reads following ENVs 
SENSORMOTE_READ_INTERVAL   - Time in Milliseconds to read temperature value

##Requirenments 
- This requies build-essential make to be installed 
- Requires systemd managar to be installed on system

##Compilation and Installation
1. Run `make RPI=1 all` to compile the sensormote.
3. [OPTIONAL] To run test go to `tests` dir and run `make run`. 
   This will compile and run the tests
2. Run `make install` as root to install sensormote to the system
3. To Run manually `build/sensormote`

##Usage 
- To start service `systemctl start sensormote.service`
- To stop service `systemctl stop sensormote.service`
- To disable service  on startup `systemctl disable sensormote.service`
- To enable service `systemctl enable sensormote.service`
- To view logs `journalctl -f -u sensormote.service`


##Improvements
1. Reread configurations on a signal
2. Make more parameters configurable 
3. Improve m2m interface , use mqtt api for communication

