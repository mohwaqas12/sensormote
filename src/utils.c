/*
 * utils.c
 *
 *  Created on: May 1, 2019
 *      Author: waqas
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Local Includes
#include "utils.h"

char * ReadEnv(const char *Name, char *Default){

	if (Name == NULL && Default == NULL)
		return NULL;
	char *Value = getenv(Name);
	if (Value != NULL)
	{
		return Value;
	}
	else
		return Default;
}

int TimeElapsed( unsigned long time1, unsigned long time2)
{
	if (time1 < time2)
		return 1 ;
	else
		return 0;
}

unsigned long GetElapsedTimeMiliSeconds()
{
	struct timespec tv= {0};
	int result = clock_gettime(CLOCK_MONOTONIC, &tv);
	if (result == -1)
	{
		printf("Could not get monotime time \n");
		return (unsigned long ) 0;
	}
	return (unsigned long) (( tv.tv_sec * MILLISECONS_IN_SECOND ) + ( tv.tv_nsec / NANOSECONDS_IN_MILLISECONDS) );
}
