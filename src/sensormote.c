/*
 * sensormote.c
 *
 *  Created on: Apr 29, 2019
 *      Author: waqas
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

//Local Includes
#include "sensormote.h"
#include "utils.h"
#include "logging.h"
#include "transport.h"
#include "tempsensor.h"

// Global Variables
volatile unsigned long ReadTTL=0;
int ReadInterval = 0;
int ShouldRun = 1;

//Signal Handlers to Catch Certain Signals
void SignalHandler(int SignalNum)
{
	if(SignalNum == SIGHUP)
		//Read Sensor reading Immediately
		ReadTTL = 0;
	else if (SignalNum == SIGTERM)
		//To shutdown gracefully
		ShouldRun = 0;
}

//Read Variables from Command Line
static int readVariables(int argc, char **argv)
{
	int opt;
	static char const help[] = {
				SENSORMOTE_APPLICATION_NAME " -- Reads Temperate and sends to a socket\n" \
				"\t\t USAGE : " SENSORMOTE_APPLICATION_NAME " [options] \n" \
				"Options: \n" \
				"\t -i value        Read interval period in MS\n"
				"\t -h              Prints help message\n"
				"\t -v              Prints version \n"
			};
	ReadInterval = atoi(ReadEnv(SENSORMOTE_READ_INTERVAL_NAME,SENSORMOTE_READ_INTERVAL_MS_DEFAULT));

	//Read arguments if any

	while((opt = getopt(argc, argv,"i:vh")) != -1)
	{
		switch (opt)
		{
			case 'i':
				ReadInterval = atoi(optarg);
				break;
			case ':':
				printf("option needs a value\n");
				return -1;
				break;
			case 'v':
				printf("%s version %s\n", SENSORMOTE_APPLICATION_NAME, SENSORMOTE_APPLICATION_VERSION);
				return -1;
				break;
			case 'h':
				printf("%s", help);
				return -1;
				break;
			case '?':
				printf("Unknown Option %c\n", optopt);
				printf("%s", help);
				return -1;
				break;
		}
	}
	return 0;
}

//Setup Signal Handlers to Catch Signals
int SetupSignals()
{
	struct sigaction HupAction, TermAction, PipeAction;
	memset(&HupAction,0,sizeof(struct sigaction));
	memset(&TermAction,0,sizeof(struct sigaction));
	memset(&PipeAction,0,sizeof(struct sigaction));
	HupAction.sa_handler = SignalHandler;
	TermAction.sa_handler = SignalHandler;
	PipeAction.sa_handler = SIG_IGN;
	sigaction(SIGHUP, &HupAction,NULL);
	sigaction(SIGTERM, &TermAction,NULL);
	sigaction(SIGPIPE, &TermAction,NULL);

	return 0;
}

int main(int argc, char *argv[])
{
	int result=0;
	unsigned long CurrentTime;
	LogOpen(SENSORMOTE_APPLICATION_NAME);
	SetupSignals();
	if ((result=readVariables(argc,argv) != 0))
	{
		LogMessage(LOG_ERR, "Exiting .\n");
		LogClose();
		exit(-1);
	}

	//Open Transport
	Transport_t  *transport = TransportInit();
	TransportOpen(transport,SENSORMOTE_HOST_DEFAULT,SENSORMOTE_PORT_DEFAULT);
	CurrentTime = GetElapsedTimeMiliSeconds();
	ReadTTL = CurrentTime + ReadInterval;

	LogMessage(LOG_INFO, "%s started.", SENSORMOTE_APPLICATION_NAME);
	//Keep running until we get a SIGTERM
	while(ShouldRun)
	{
		if (TimeElapsed(ReadTTL,CurrentTime))
		{
			if(ReadTTL==0)
			{
				LogMessage(LOG_DEBUG,"SIGHUP requested read\n");
			}
			//Time to read the Value
			int Temp = ReadTemp();
			LogMessage(LOG_DEBUG,"Read Temperature : %d\n",Temp);
			//TODO : Use JSON LIB to send data in JSON format
			char message[50]= "";
			snprintf(message,50,"{ \"cpu\":\"%s\",\"temp\":\"%d\"}","cpu0", Temp);

			if (transport->LastError)
			{
				TransportClose(transport);
				TransportOpen(transport,SENSORMOTE_HOST_DEFAULT,SENSORMOTE_PORT_DEFAULT);
			}

			TransportWrite(transport,message);
			//Update the TTL for next read
			ReadTTL = CurrentTime + ReadInterval;
		}

		LogMessage(LOG_DEBUG,"Sleeping for %u %s\n", SENSORMOTE_SLEEP_TIME_MILLISECONDS, "ms");
		usleep(SENSORMOTE_SLEEP_TIME_MILLISECONDS * 1000);

		//Update the time
		CurrentTime = GetElapsedTimeMiliSeconds();
	}

	//Close the Transport
	TransportClose(transport);
	TransportDeinit(&transport);

	LogMessage(LOG_INFO,"Terminating gracefully");

	LogClose();
	return 0;
}
