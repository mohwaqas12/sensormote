/*
 * transport.c
 *
 *  Created on: May 1, 2019
 *      Author: waqas
 */

#include "transport.h"

Transport_t * TransportInit()
{
	Transport_t *transport = (Transport_t*)malloc( sizeof(struct Transport));
	if (transport == NULL)
		return NULL;

	memset((char*)transport,0,sizeof(transport));
	return transport;
}

void TransportDeinit(Transport_t** transport)
{
	//Release resources
	if(transport != NULL)
	{
		if( *transport != NULL)
		{
			free(*transport);
			*transport = NULL;
		}
	}
}

int TransportOpen(Transport_t *transport,char* server,int port)
{
	if (transport == NULL || server == NULL || port == 0)
	{
		return -1;
	}
	transport->LastError = 0;
	transport->SockFd = socket(AF_INET,SOCK_STREAM,0);
	transport->server = gethostbyname(server);
	memset((char*)&transport->serv_addr,0, sizeof(struct sockaddr_in));
	transport->serv_addr.sin_family = AF_INET;
	memcpy((char*)&transport->serv_addr.sin_addr,(char*)transport->server->h_addr,transport->server->h_length);
	transport->serv_addr.sin_port = htons(port);

	if ( connect(transport->SockFd,(const struct sockaddr*)&transport->serv_addr,sizeof(transport->serv_addr)) != 0 )
	{
		transport->LastError = 1;
		return -1;
	}

	return 0;
}

int TransportWrite(Transport_t *transport,char *message)
{
	int Result = write(transport->SockFd, message,strlen(message));
	if ( Result < 0 )
		transport->LastError = 1;
	return Result;
}

int TransportClose(Transport_t *transport)
{
	if(transport == NULL)
	{
		return -1;
	}
		shutdown (transport->SockFd,2);
		close(transport->SockFd);
		return 0;
}
