/*
 * transport.h
 *
 *  Created on: May 1, 2019
 *      Author: waqas
 */

#ifndef SRC_INCLUDE_TRANSPORT_H_
#define SRC_INCLUDE_TRANSPORT_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

typedef struct Transport
{
	int Type;
	int SockFd;
	int PortNum;
	int LastError;
	struct sockaddr_in serv_addr;
	struct hostent *server;
}Transport_t;

Transport_t * TransportInit();
void TransportDeinit(Transport_t** transport);
int TransportOpen(Transport_t *transport,char* server,int port);
int TransportWrite(Transport_t *transport,char*message);
int TransportClose(Transport_t *transport);

#endif /* SRC_INCLUDE_TRANSPORT_H_ */
