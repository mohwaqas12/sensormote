/*
 * logging.h
 *
 *  Created on: May 1, 2019
 *      Author: waqas
 */

#ifndef SRC_INCLUDE_LOGGING_H_
#define SRC_INCLUDE_LOGGING_H_

#include <syslog.h>

int  LogOpen(char*name);
void LogMessage(int type, const char*message, ...);
void LogClose();

#endif /* SRC_INCLUDE_LOGGING_H_ */
