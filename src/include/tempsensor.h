/*
 * tempsensor.h
 *
 *  Created on: May 1, 2019
 *      Author: waqas
 */

#ifndef SRC_INCLUDE_TEMPSENSOR_H_
#define SRC_INCLUDE_TEMPSENSOR_H_


#ifdef RPI
	#define TEMPSENSOR_PATH "/sys/class/thermal/thermal_zone0/temp"
#else
	#define TEMPSENSOR_PATH "tests/tempsensor"
#endif

int ReadTemp();


#endif /* SRC_INCLUDE_TEMPSENSOR_H_ */
