/*
 * utils.h
 *
 *  Created on: May 1, 2019
 *      Author: waqas
 */

#ifndef SRC_INCLUDE_UTILS_H_
#define SRC_INCLUDE_UTILS_H_

//Local Defines
#define MILLISECONS_IN_SECOND		1000UL
#define NANOSECONDS_IN_MILLISECONDS 1000000UL

//Function Prototypes
char * ReadEnv(const char *Name, char *Default);
unsigned long GetElapsedTimeMiliSeconds();
int TimeElapsed( unsigned long time1, unsigned long time2);

#endif /* SRC_INCLUDE_UTILS_H_ */
