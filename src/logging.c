/*
 * logging.c
 *
 *  Created on: May 1, 2019
 *      Author: waqas
 */

#include <stdio.h>
#include <stdarg.h>

//Local Include
#include "logging.h"

static int LogOpened=0;

int LogOpen(char*Name)
{
	openlog(Name, LOG_CONS | LOG_PID | LOG_NDELAY, LOG_USER);
	LogOpened = 1;
	return 0;
}

void LogMessage(int type, const char*Message, ...)
{
	va_list args;
	va_start(args,Message);
	vsyslog(type, Message,args);
	va_end(args);
}

void LogClose()
{
	closelog();
	LogOpened=0;
}
