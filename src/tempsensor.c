/*
 * tempsensor.c
 *
 *  Created on: May 1, 2019
 *      Author: waqas
 */

#include <stdio.h>
#include "tempsensor.h"

int ReadTemp()
{
	int TempValue = 0;
	FILE *f = fopen(TEMPSENSOR_PATH,"r");
	if(f != NULL){
		fscanf(f,"%d",&TempValue);
		fclose(f);
	}
	return TempValue;
}
